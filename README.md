# Voucher Service

# Installation

### 1. Clone the repository

```
git clone git@bitbucket.org:aharen/vourcher-service.git
```

`cd` into project folder 

```
cd vourcher-service
```

### 2. Run the install command (requires that you have bash on your local machine)

```
bash scripts/install.sh
```

If you don't have bash on local machine:

* create `.env` file from `/.env.example` file in root folder
* create `.env` file from `/src/.env.example` file in src folder
* `cd` in to `src` folder and run `composer instll`

### 3. Update the environment files at `.env` and `src/.env` files - optional

The MYSQL values (`DOCKER_MYSQL_*`) from Docker environment file (located at root folder) must match the `DB_*` values in Lumen environment file

### 4. Update application key (APP_KEY) of Lumen environment file (`src/.env`) - optional

The following command will generate a random key string, you can simply copy it and add to `APP_KEY` in Lumen environment file

```
php -r "echo 'bse64:' . base64_encode(random_bytes(32));"
```

### 5. Start the app containers

```
docker-compose up voucher-app -d
```

### 6. Migrate the database

```
docker-compose run --rm artisan migrate
```

Assuming that no ports was modified, you should be able to visit the running app at http://localhost:4000


### Run Tests

```
docker-compose run --rm php /var/www/html/vendor/bin/phpunit
```