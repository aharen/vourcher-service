#!/usr/bin/env bash

# Check docker .env files exist, create them if doesnt exsit
if [ ! -f ".env" ]; then
    cp .env.example .env
    echo 'Docker .env file created'
else
    echo 'Docker .env file exists'
fi

# Check Lumen .env files exist, create them if doesnt exsit
if [ ! -f "./src/.env" ]; then
    cp ./src/.env.example ./src/.env
    echo 'Lumen .env file created'
else
    echo 'Lumen .env file exists'
fi

# Check for vendor folder, run composer install if not exist
if [ ! -d "./src/vendor" ]; then
    echo 'Running composer install'
    
    docker-compose run \
        --rm \
        composer \
        install
else
    echo 'Vendor file was found'
fi