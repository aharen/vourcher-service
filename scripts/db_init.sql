CREATE DATABASE IF NOT EXISTS `vouchers` DEFAULT CHARACTER SET = utf8mb4 DEFAULT COLLATE = utf8mb4_unicode_ci;

GRANT ALL ON `vouchers`.* TO 'voucherroot'@'%' ;

FLUSH PRIVILEGES;