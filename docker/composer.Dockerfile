FROM composer:2

ARG PHP_GROUP
ARG PHP_USER

ENV PHP_GROUP=${PHP_GROUP}
ENV PHP_USER=${PHP_USER}

RUN addgroup --system ${PHP_GROUP}; exit 0
RUN adduser --system -G ${PHP_GROUP} -s /bin/sh -D ${PHP_USER}; exit 0

WORKDIR /var/www/html