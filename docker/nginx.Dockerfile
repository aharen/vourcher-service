FROM nginx:stable-alpine

ARG NGINX_USER
ARG NGINX_GROUP

ENV NGINX_USER=${NGINX_USER}
ENV NGINX_GROUP=${NGINX_GROUP}

RUN addgroup --system ${NGINX_GROUP}; exit 0
RUN adduser --system -G ${NGINX_GROUP} -s /bin/sh -D ${NGINX_USER}; exit 0

RUN mkdir -p /var/www/html/public

ADD nginx/default.conf /etc/nginx/conf.d/default.conf

RUN sed -i "s/user www-data/user ${NGINX_USER}/g" /etc/nginx/nginx.conf