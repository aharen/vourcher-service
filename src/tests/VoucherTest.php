<?php

use App\Services\CreateVoucherService;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class VoucherTest extends TestCase
{
    use DatabaseMigrations;
    
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_validation()
    {
        $this->expectException(ValidationException::class);

        (new CreateVoucherService())
            ->create([]);
    }

    public function test_create_voucher()
    {
        $voucher = (new CreateVoucherService())
            ->create([
                'order_uuid' => 'd04c5bc8-87e5-4923-a80b-c42ecce4ad2b',
                'email' => 'hello@world.com',
                'total' => 10000,
            ]);

        $this->seeInDatabase('vouchers', [
            'order_uuid' => 'd04c5bc8-87e5-4923-a80b-c42ecce4ad2b',
            'voucher_id' => $voucher->voucher_id,
            'value' => 500,
            'used' => 0,
        ]);
    }
}
