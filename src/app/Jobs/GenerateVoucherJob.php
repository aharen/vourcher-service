<?php

namespace App\Jobs;

use App\Services\CreateVoucherService;
use Illuminate\Support\Facades\Log;
use Throwable;

class GenerateVoucherJob extends Job
{
    private array $order;

    public function handle()
    {
        (new CreateVoucherService())
            ->create($this->order);
    }

    public function failed(Throwable $th)
    {
        Log::error($th->getMessage());
    }
}
