<?php

namespace App\Services;

use App\Models\Voucher;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class CreateVoucherService
{
    public function create(array $order): Voucher
    {
        $validated = $this->validate($order);

        return Voucher::create([
            'order_uuid' => $validated['order_uuid'],
            'voucher_id' => Str::uuid(),
            'value' => 500,
            'used' => false,
        ]);
    }

    private function validate($data)
    {
        $validator = Validator::make($data, [
            'order_uuid' => [
                'required',
                'uuid',
                'unique:vouchers,order_uuid'
            ],
            'email' => [
                'required',
                'email',
            ],
            'total' => [
                'required',
                'min:10000',
                'integer',
            ]
        ]);

        if ($validator->fails()) {
            throw new ValidationException(422);
        }

        return $validator->validated();
    }
}
