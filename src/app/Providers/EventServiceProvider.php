<?php

namespace App\Providers;

use App\Jobs\GenerateVoucherJob;
use Illuminate\Support\Facades\App;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    public function boot()
    {
        App::bindMethod(
            [
                GenerateVoucherJob::class,
                'handle',
            ],
            fn ($job) => $job->handle(),
        );
    }
}
