<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    protected $table = 'vouchers';

    protected $fillable = [
        'order_uuid',
        'voucher_id',
        'value',
        'used',
    ];

    protected $casts = [
        'used' => 'boolean',
        'order_uuid' => 'string',
        'voucher_id' => 'string',
    ];
}
